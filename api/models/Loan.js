/**
 * Loan.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    loan: {
      type: "integer",
      defaultsTo: -1
    },

    estApprove: {
      type: "boolean",
      required: true
    },

    approve: {
      type: "boolean",
      defaultsTo: false
    },

    owner: {
      model: "user"
    }
  }
};

