/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: "string",
      required: true
    },

    password: {
      type: "string",
      required: true
    },

    income: {
      type: "double",
      required: true
    },

    estExpenses: {
      type: "double",
      required: true
    },

    loans: {
      collection: "loan",
      via: "owner"
    }
  }
};

