module.exports = {
  isApprovable: function(income, estExpenses, loanCost) {
    if(0.7* income - estExpenses > loanCost) return true;

    return false;
  }
};
